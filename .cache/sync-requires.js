const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/var/www/html/Amir/blog/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/var/www/html/Amir/blog/src/pages/404.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/var/www/html/Amir/blog/src/pages/index.js"))),
  "component---src-pages-page-2-js": hot(preferDefault(require("/var/www/html/Amir/blog/src/pages/page-2.js"))),
  "component---src-pages-using-typescript-tsx": hot(preferDefault(require("/var/www/html/Amir/blog/src/pages/using-typescript.tsx"))),
  "component---src-templates-article-js": hot(preferDefault(require("/var/www/html/Amir/blog/src/templates/article.js")))
}

